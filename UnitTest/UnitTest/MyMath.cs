﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest
{
    public static class MyMath
    {
        public static int GetMax(int[] source)
        {
            int max = source[0];
            foreach (int item in source)
            {
                if (item > max)
                {
                    max = item;
                }
            }
            return max;
        }
        public static List<int> GetPrimes(int[] source)
        {
            bool isPrime = true;
            var list = new List<int>(5);
            for (int i = 0; i < source.Length; i++)
            {
                if (source[i] % 2 == 0 && Math.Abs(source[i]) != 2)
                {
                    isPrime = false;
                }
                else
                {
                    for (int j = 3; j <= Math.Sqrt(source[i]); j+=2)
                    {
                        if (Math.Sqrt(source[i]) % j == 0)
                        {
                            isPrime = false;
                        }
                    }
                }
                if (isPrime && list.Count < 5)
                {
                    list.Add(source[i]);
                }
                isPrime = true;
            }
            return list;
        }
    }
}
