﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = { 1, -2, 3, 4, 5, 6, 7, 8, 9, 10, 127 };
            int max = MyMath.GetMax(arr);
            var list = MyMath.GetPrimes(arr);

            Console.ReadKey();
        }
    }
}
